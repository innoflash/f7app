define(["app", "js/index/indexView"], function (app, View) {
    var $ = jQuery;
    var $$ = Dom7;
    var user = {};

    var bindings = [
        {
            element: '#btnFbSignin',
            event: 'click',
            handler: fbAuthentication
        }
    ];


    function init() {
        console.log(app);
        View.render({
            bindings: bindings
        });
    }

    function fbAuthentication() {

    }

    function onOut() {
        console.log('index outting');
    }

    return {
        init: init,
        onOut: onOut,
        reinit: init
    };
});